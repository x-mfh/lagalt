﻿using API.Models.DTO.ProjectDTO;
using API.Models.DTO.SkillDTO;
using API.Models.DTO.UserDTO;
using System;
using System.Collections.Generic;

namespace API.Models.DTO.ProjectApplicationDTO
{
    public class ProjectApplicationReadDTO
    {
        public int Id { get; set; }
        public string MotivationalText { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsPending { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }

        public UserRefReadDTO User { get; set; }
        public ProjectRefReadDTO Project { get; set; }
        public List<SkillReadDTO> Skills { get; set; }
    }
}
