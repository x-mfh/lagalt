﻿using System;
using System.Collections.Generic;

namespace API.Models.DTO.ProjectApplicationDTO
{
    public class ProjectApplicationCreateDTO
    {
        public string MotivationalText { get; set; }
        public List<int> Skills { get; set; }
    }
}
