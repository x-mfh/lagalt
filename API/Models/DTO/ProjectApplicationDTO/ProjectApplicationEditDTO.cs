﻿using System;
using System.Collections.Generic;

namespace API.Models.DTO.ProjectApplicationDTO
{
    public class ProjectApplicationEditDTO
    {
        public int Id { get; set; }
        public string MotivationalText { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsActive { get; set; }
        public bool IsPending { get; set; }
        public Guid UserId { get; set; }
        public int ProjectId { get; set; }
        //public List<int> Skills { get; set; }
    }
}
