﻿using System;

namespace API.Models.DTO.UserDTO
{
    public class UserEditDTO
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsSkillsHidden { get; set; }
        public bool IsActive { get; set; }
    }
}
