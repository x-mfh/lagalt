﻿using System;

namespace API.Models.DTO.UserDTO
{
    public class UserRefReadDTO
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
    }
}
