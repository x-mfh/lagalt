﻿using System;

namespace API.Models.DTO.UserDTO
{
    public class UserCreateDTO
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsSkillsHidden { get; set; }
    }
}
