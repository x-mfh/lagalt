﻿using API.Models.DTO.SkillDTO;
using API.Models.DTO.UserPortfolioDTO;
using System;
using System.Collections.Generic;

namespace API.Models.DTO.UserDTO
{
    public class UserReadDTO
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsSkillsHidden { get; set; }
        public bool IsActive { get; set; }
        public List<SkillReadDTO> Skills { get; set; }
        public List<UserPortfolioReadDTO> UserPortfolios { get; set; }
    }
}
