﻿using System;

namespace API.Models.DTO.UserPortfolioDTO
{
    public class UserPortfolioEditDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PortfolioURL { get; set; }
        //public Guid UserId { get; set; }
    }
}
