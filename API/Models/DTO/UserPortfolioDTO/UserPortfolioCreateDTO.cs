﻿using System;

namespace API.Models.DTO.UserPortfolioDTO
{
    public class UserPortfolioCreateDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PortfolioURL { get; set; }
        public Guid UserId { get; set; }
    }
}
