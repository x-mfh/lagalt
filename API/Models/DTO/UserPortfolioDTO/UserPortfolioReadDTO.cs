﻿namespace API.Models.DTO.UserPortfolioDTO
{
    public class UserPortfolioReadDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PortfolioURL { get; set; }
    }
}
