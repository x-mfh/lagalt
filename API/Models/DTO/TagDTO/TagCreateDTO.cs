﻿namespace API.Models.DTO.TagDTO
{
    public class TagCreateDTO
    {
        public string Name { get; set; }
        public int SkillId { get; set; }
    }
}
