﻿namespace API.Models.DTO.TagDTO
{
    public class TagEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SkillId { get; set; }
    }
}
