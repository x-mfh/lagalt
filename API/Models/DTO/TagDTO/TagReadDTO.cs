﻿namespace API.Models.DTO.TagDTO
{
    public class TagReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SkillName { get; set; }
    }
}
