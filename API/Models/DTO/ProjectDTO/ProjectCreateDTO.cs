﻿using System.Collections.Generic;

namespace API.Models.DTO.ProjectDTO
{
    public class ProjectCreateDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string RepoURL { get; set; }
        public string ImageURL { get; set; }
        public int ProgressId { get; set; }
        public List<ProjectSkillCreateDTO> Skills { get; set; }
    }
}
