﻿using System;

namespace API.Models.DTO.ProjectDTO
{
    public class ProjectEditDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string RepoURL { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public int ProgressId { get; set; }
    }
}
