﻿namespace API.Models.DTO.ProjectDTO
{
    public class ProjectSkillReadDTO
    {
        public string SkillName { get; set; }
        public int RequiredCount { get; set; }
        public int FoundCount { get; set; }
    }
}
