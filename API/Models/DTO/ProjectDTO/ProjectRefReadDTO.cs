﻿namespace API.Models.DTO.ProjectDTO
{
    public class ProjectRefReadDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
