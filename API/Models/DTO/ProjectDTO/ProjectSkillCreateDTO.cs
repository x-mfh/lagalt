﻿namespace API.Models.DTO.ProjectDTO
{
    public class ProjectSkillCreateDTO
    {
        public int SkillId { get; set; }
        public int RequiredCount { get; set; }
    }
}
