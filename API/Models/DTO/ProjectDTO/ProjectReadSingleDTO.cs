﻿using API.Models.DTO.ProjectProgressDTO;
using API.Models.DTO.UserDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.DTO.ProjectDTO
{
    public class ProjectReadSingleDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string RepoURL { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public UserRefReadDTO Creator { get; set; }
        public ProjectProgressReadDTO Progress { get; set; }
        public List<ProjectSkillReadDTO> Skills { get; set; }
        public List<string> Tags { get; set; }
    }
}
