﻿namespace API.Models.DTO.MessageDTO
{
    public class MessageCreateDTO
    {
        public string Text { get; set; }
        public int? MessageId { get; set; }
    }
}
