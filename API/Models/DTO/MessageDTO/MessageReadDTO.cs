﻿using API.Models.DTO.UserDTO;
using System;

namespace API.Models.DTO.MessageDTO
{
    public class MessageReadDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public UserRefReadDTO User { get; set; }
        public int? MessageId { get; set; }
    }
}
