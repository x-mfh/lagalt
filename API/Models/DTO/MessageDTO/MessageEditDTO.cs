﻿namespace API.Models.DTO.MessageDTO
{
    public class MessageEditDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
    }
}
