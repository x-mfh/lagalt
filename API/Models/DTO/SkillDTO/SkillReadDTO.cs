﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.DTO.SkillDTO
{
    public class SkillReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
