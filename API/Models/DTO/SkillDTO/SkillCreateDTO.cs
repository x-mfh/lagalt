﻿namespace API.Models.DTO.SkillDTO
{
    public class SkillCreateDTO
    {
        public string Name { get; set; }
    }
}
