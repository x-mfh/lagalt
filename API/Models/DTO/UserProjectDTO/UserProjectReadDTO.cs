﻿using API.Models.DTO.ProjectDTO;
using API.Models.DTO.SkillDTO;
using API.Models.DTO.UserDTO;
using System;
using System.Collections.Generic;

namespace API.Models.DTO.UserProjectDTO
{
    public class UserProjectReadDTO
    {
        public int Id { get; set; }
        public ProjectRefReadDTO Project { get; set; }
        public UserRefReadDTO User { get; set; }
        public UserProjectRoleReadDTO ProjectRole { get; set; }
        public List<SkillReadDTO> Skills { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
