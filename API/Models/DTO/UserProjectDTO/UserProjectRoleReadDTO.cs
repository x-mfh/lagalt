﻿namespace API.Models.DTO.UserProjectDTO
{
    public class UserProjectRoleReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
