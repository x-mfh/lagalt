﻿using API.Models.DTO.SkillDTO;
using System;
using System.Collections.Generic;

namespace API.Models.DTO.UserProjectDTO
{
    public class UserProjectCreateDTO
    {
        public Guid UserId { get; set; }
        //public int UserProjectRoleId { get; set; }
        // Change at some point??
        public List<SkillReadDTO> Skills { get; set; }
    }
}
