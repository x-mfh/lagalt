﻿using System;
using System.Collections.Generic;

namespace API.Models.DTO.UserProjectDTO
{
    public class UserProjectEditDTO
    {
        public Guid UserId { get; set; }
        public List<int> Skills { get; set; }
        public int UserProjectRoleId { get; set; }
    }
}
