﻿namespace API.Models.DTO.ProjectProgressDTO
{
    public class ProjectProgressCreateDTO
    {
        public string Name { get; set; }
    }
}
