﻿namespace API.Models.DTO.ProjectProgressDTO
{
    public class ProjectProgressEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
