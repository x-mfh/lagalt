﻿namespace API.Models.DTO.ProjectProgressDTO
{
    public class ProjectProgressReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
