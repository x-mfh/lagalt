﻿namespace API.Models.DTO.UserHistoryActionDTO
{
    public class UserHistoryActionEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
