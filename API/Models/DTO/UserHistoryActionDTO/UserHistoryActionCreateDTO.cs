﻿namespace API.Models.DTO.UserHistoryActionDTO
{
    public class UserHistoryActionCreateDTO
    {
        public string Name { get; set; }
    }
}
