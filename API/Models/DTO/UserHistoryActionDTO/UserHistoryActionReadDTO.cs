﻿namespace API.Models.DTO.UserHistoryActionDTO
{
    public class UserHistoryActionReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
