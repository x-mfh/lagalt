﻿using API.Models.DTO.ProjectDTO;
using System;

namespace API.Models.DTO.UserHistoryDTO
{
    public class UserHistoryReadDTO
    {
        public int Id { get; set; }
        public ProjectRefReadDTO Project { get; set; }
        public string UserHistoryAction { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
