﻿using System;

namespace API.Models.DTO.UserHistoryDTO
{
    public class UserHistoryCreateDTO
    {
        public Guid UserId { get; set; }
        public int ProjectId { get; set; }
        public int UserHistoryActionId { get; set; }
    }
}
