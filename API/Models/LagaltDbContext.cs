﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace API
{
    public class LagaltDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectProgress> ProjectProgresses { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<ProjectSkill> ProjectSkills { get; set; }
        public DbSet<UserHistory> UserHistories { get; set; }
        public DbSet<UserHistoryAction> UserHistoryActions { get; set; }
        public DbSet<UserPortfolio> UserPortfolios { get; set; }
        public DbSet<UserProject> UserProjects { get; set; }
        public DbSet<UserProjectRole> UserProjectRoles { get; set; }
        public DbSet<ProjectApplication> ProjectApplications { get; set; }


        public LagaltDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region SkillData
            modelBuilder.Entity<Skill>().HasData(
                new Skill
                {
                    Id = 1,
                    Name = "web development"
                },
                new Skill
                {
                    Id = 2,
                    Name = "game development"
                },
                new Skill
                {
                    Id = 3,
                    Name = "film"
                },
                new Skill
                {
                    Id = 4,
                    Name = "music"
                }
            );
            #endregion

            #region TagData
            modelBuilder.Entity<Tag>().HasData(
                new Tag
                {
                    Id = 1,
                    Name = "React",
                    SkillId = 1
                },
                new Tag
                {
                    Id = 2,
                    Name = "CSS",
                    SkillId = 1
                },
                new Tag
                {
                    Id = 3,
                    Name = "Unity",
                    SkillId = 2
                },
                new Tag
                {
                    Id = 4,
                    Name = "Singer",
                    SkillId = 4
                }
            );
            #endregion

            #region ProjectProgressData
            modelBuilder.Entity<ProjectProgress>().HasData(
                new ProjectProgress
                {
                    Id = 1,
                    Name = "Founding"
                },
                new ProjectProgress
                {
                    Id = 2,
                    Name = "In progress"
                },
                new ProjectProgress
                {
                    Id = 3,
                    Name = "Stalled"
                },
                new ProjectProgress
                {
                    Id = 4,
                    Name = "Completed"
                }
            );
            #endregion

            #region UserHistoryActionData
            modelBuilder.Entity<UserHistoryAction>().HasData(
                new UserHistoryAction
                {
                    Id = 1,
                    Name = "Viewed"
                },
                new UserHistoryAction
                {
                    Id = 2,
                    Name = "Applied"
                },
                new UserHistoryAction
                {
                    Id = 3,
                    Name = "Contributed"
                }
            );
            #endregion

            #region UserData
            //modelBuilder.Entity<User>().HasData(
            //    new User
            //    {
            //        Id = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        Email = "test@test.test",
            //        Name = "Lagalt Tester",
            //        Username = "Test1",
            //        Description = "Im a test user1",
            //        ImageUrl = null,
            //        IsSkillsHidden = false,
            //        CreatedAt = new DateTime(2021, 6, 4),
            //        IsActive = true
            //    },
            //    new User
            //    {
            //        Id = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        Email = "test2@test.test",
            //        Name = "Test2 test",
            //        Username = "Test2",
            //        Description = "Im a test user2",
            //        ImageUrl = null,
            //        IsSkillsHidden = false,
            //        CreatedAt = new DateTime(2021, 6, 5),
            //        IsActive = true
            //    },
            //    new User
            //    {
            //        Id = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        Email = "test3@test.test",
            //        Name = "Test3 test",
            //        Username = "Test3",
            //        Description = "Im a test user3",
            //        ImageUrl = null,
            //        IsSkillsHidden = true,
            //        CreatedAt = new DateTime(2021, 6, 5),
            //        IsActive = true
            //    },
            //    new User
            //    {
            //        Id = Guid.Parse("444df75e-83c0-4535-b567-9d786886db2f"),
            //        Email = "test4@test.test",
            //        Name = "Test4 test",
            //        Username = "Test4",
            //        Description = "Im a test user4",
            //        ImageUrl = null,
            //        IsSkillsHidden = true,
            //        CreatedAt = new DateTime(2021, 6, 5),
            //        IsActive = true
            //    }
            //);
            #endregion

            #region SkillUserData
            //user 1 web
            //user 2 game and web
            //user 3 game and web
            //modelBuilder.Entity<User>()
            //    .HasMany(u => u.Skills)
            //    .WithMany(s => s.Users)
            //    .UsingEntity(j => j.HasData(
            //        new { UsersId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"), SkillsId = 1 },
            //        new { UsersId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"), SkillsId = 1 },
            //        new { UsersId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"), SkillsId = 2 },
            //        new { UsersId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"), SkillsId = 1 },
            //        new { UsersId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"), SkillsId = 2 }
            //    ));
            #endregion

            #region ProjectData
            modelBuilder.Entity<Project>()
                .HasOne(p => p.Creator)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);
            //modelBuilder.Entity<Project>().HasData(
            //    new Project
            //    {
            //        Id = 1,
            //        Title = "Help develop this site!",
            //        Description = "We need a lot of different people with different skills",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProgressId = 2
            //    },
            //    new Project
            //    {
            //        Id = 2,
            //        Title = "Making a website game",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        ProgressId = 1
            //    }
            //    ,
            //    new Project
            //    {
            //        Id = 3,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 4,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("444df75e-83c0-4535-b567-9d786886db2f"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 5,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 6,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("444df75e-83c0-4535-b567-9d786886db2f"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 7,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    }
            //    ,
            //    new Project
            //    {
            //        Id = 8,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 9,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 10,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 11,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 12,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    }
            //    ,
            //    new Project
            //    {
            //        Id = 13,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 14,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 15,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 16,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 17,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    }
            //    ,
            //    new Project
            //    {
            //        Id = 18,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 19,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    },
            //    new Project
            //    {
            //        Id = 20,
            //        Title = "Making a community to for creative people!",
            //        Description = "Need people with creative minds",
            //        RepoURL = null,
            //        ImageURL = null,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6),
            //        CreatorId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProgressId = 1
            //    }
            //);
            #endregion

            #region ProjectTagData
            //modelBuilder.Entity<Project>()
            //    .HasMany(p => p.Tags)
            //    .WithMany(t => t.Projects)
            //    .UsingEntity(j => j.HasData(
            //        new { ProjectsId = 1, TagsId = 1 },
            //        new { ProjectsId = 1, TagsId = 2 },
            //        new { ProjectsId = 2, TagsId = 1 },
            //        new { ProjectsId = 2, TagsId = 2 },
            //        new { ProjectsId = 2, TagsId = 3 }
            //    ));
            #endregion

            #region ProjectSkillData
            modelBuilder.Entity<ProjectSkill>().HasKey(ps => new { ps.ProjectId, ps.SkillId });
            //modelBuilder.Entity<ProjectSkill>().HasData(
            //    new ProjectSkill
            //    {
            //        ProjectId = 1,
            //        SkillId = 1,
            //        RequiredCount = 10,
            //        FoundCount = 1
            //    },
            //    new ProjectSkill
            //    {
            //        ProjectId = 1,
            //        SkillId = 4,
            //        RequiredCount = 1,
            //        FoundCount = 0
            //    },
            //    new ProjectSkill
            //    {
            //        ProjectId = 2,
            //        SkillId = 1,
            //        RequiredCount = 2,
            //        FoundCount = 1
            //    },
            //    new ProjectSkill
            //    {
            //        ProjectId = 2,
            //        SkillId = 2,
            //        RequiredCount = 2,
            //        FoundCount = 1
            //    }
            //);
            #endregion

            #region UserProjectRoleData
            modelBuilder.Entity<UserProjectRole>().HasData(
                new UserProjectRole
                {
                    Id = 1,
                    Name = "Owner"
                },
                new UserProjectRole
                {
                    Id = 2,
                    Name = "Admin"
                },
                new UserProjectRole
                {
                    Id = 3,
                    Name = "Contributor"
                }
            );
            #endregion

            #region UserProjectData
            modelBuilder.Entity<UserProject>().HasIndex(up => new { up.UserId, up.ProjectId }).IsUnique();
            //modelBuilder.Entity<UserProject>().HasData(
            //    #region Project1
            //    new UserProject
            //    {
            //        Id = 1,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        UserProjectRoleId = 1,
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    },
            //    new UserProject
            //    {
            //        Id = 2,
            //        UserId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        ProjectId = 1,
            //        UserProjectRoleId = 2,
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    }
            //    #endregion
            //    #region Project2
            //    ,
            //    new UserProject
            //    {
            //        Id = 3,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 2,
            //        UserProjectRoleId = 1,
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    },
            //    new UserProject
            //    {
            //        Id = 4,
            //        UserId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        ProjectId = 2,
            //        UserProjectRoleId = 1,
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    }
            //    #endregion
            //);
            #endregion

            #region ProjectApplication
            //modelBuilder.Entity<ProjectApplication>().HasData(
            //#region Project1
            //    new ProjectApplication
            //    {
            //        Id = 1,
            //        UserId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        ProjectId = 1,
            //        IsAccepted = true,
            //        IsActive = true,
            //        MotivationalText = "I have a lot of experience with this kinda work!",
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    },
            //    new ProjectApplication
            //    {
            //        Id = 2,
            //        UserId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProjectId = 1,
            //        IsAccepted = false,
            //        IsActive = true,
            //        MotivationalText = "I'm creative!",
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    },
            //    new ProjectApplication
            //    {
            //        Id = 3,
            //        UserId = Guid.Parse("444df75e-83c0-4535-b567-9d786886db2f"),
            //        ProjectId = 1,
            //        IsAccepted = false,
            //        IsActive = true,
            //        MotivationalText = "I'm also creative!",
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    }
            //    #endregion
            //#region Project2
            //    ,
            //    new ProjectApplication
            //    {
            //        Id = 4,
            //        UserId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        ProjectId = 2,
            //        IsAccepted = true,
            //        IsActive = true,
            //        MotivationalText = "I have a lot of experience with this kinda work aswell!",
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    },
            //    new ProjectApplication
            //    {
            //        Id = 5,
            //        UserId = Guid.Parse("855cecfb-8bf5-468f-a522-6ec04268af80"),
            //        ProjectId = 2,
            //        IsAccepted = false,
            //        IsActive = true,
            //        MotivationalText = "I have created multiple web games.",
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    }
            //    #endregion
            //);
            #endregion

            // not done
            #region UserHistoryData
            //modelBuilder.Entity<UserHistory>().HasData(
            //    new UserHistory
            //    {
            //        Id = 1,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        UserHistoryActionId = 1,
            //        CreatedAt = new DateTime(2021, 6, 4)
            //    },
            //    new UserHistory
            //    {
            //        Id = 2,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        UserHistoryActionId = 2,
            //        CreatedAt = new DateTime(2021, 6, 4)
            //    },
            //    new UserHistory
            //    {
            //        Id = 3,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        UserHistoryActionId = 3,
            //        CreatedAt = new DateTime(2021, 6, 4)
            //    },
            //    new UserHistory
            //    {
            //        Id = 4,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        UserHistoryActionId = 1,
            //        CreatedAt = new DateTime(2021, 6, 5)
            //    },
            //    new UserHistory
            //    {
            //        Id = 5,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        UserHistoryActionId = 1,
            //        CreatedAt = new DateTime(2021, 6, 4)
            //    }
            //);
            #endregion
            // not done
            #region MessageData
            //modelBuilder.Entity<Message>().HasData(
            //    new Message
            //    {
            //        Id = 1,
            //        Text = "Hello World",
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 4)
            //    },
            //    new Message
            //    {
            //        Id = 2,
            //        Text = "Reply to Hello World",
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        MessageId = 1,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 5)
            //    },
            //    new Message
            //    {
            //        Id = 3,
            //        Text = "Hello World edited version",
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        ModifiedAt = new DateTime(2021, 6, 7),
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 6)
            //    },
            //    new Message
            //    {
            //        Id = 4,
            //        Text = "I am from another user",
            //        UserId = Guid.Parse("ff1c959e-ed5e-4ca4-84bb-70a5e9429507"),
            //        ProjectId = 1,
            //        IsActive = true,
            //        CreatedAt = new DateTime(2021, 6, 5)
            //    },
            //    new Message
            //    {
            //        Id = 5,
            //        Text = "I should be deleted",
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //        ProjectId = 1,
            //        IsActive = false,
            //        CreatedAt = new DateTime(2021, 6, 5)
            //    }
            //);
            #endregion

            #region UserPortfolioData
            //modelBuilder.Entity<UserPortfolio>().HasData(
            //    new UserPortfolio
            //    {
            //        Id = 1,
            //        Title = "My first project",
            //        Description = "This was a test project :)",
            //        PortfolioURL = null,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //    },
            //    new UserPortfolio
            //    {
            //        Id = 2,
            //        Title = "My second project",
            //        Description = "This is my current project im working on",
            //        PortfolioURL = null,
            //        UserId = Guid.Parse("1c016959-b3cb-42ae-a887-cf26040a95c0"),
            //    }
            //);
            #endregion

            #region ProjectApplicationSkillData
            //modelBuilder.Entity<ProjectApplication>()
            //    .HasMany(pa => pa.Skills)
            //    .WithMany(s => s.ProjectApplications)
            //    .UsingEntity(j => j.HasData(
            //        new { ProjectApplicationsId = 1, SkillsId = 1 }
            //    ));
            #endregion

            #region UserProjectSkillData
            //modelBuilder.Entity<UserProject>()
            //    .HasMany(pa => pa.Skills)
            //    .WithMany(s => s.UserProjects)
            //    .UsingEntity(j => j.HasData(
            //        new { UserProjectsId = 1, SkillsId = 1 },
            //        new { UserProjectsId = 1, SkillsId = 2 }
            //    ));
            #endregion
        }
    }
}
