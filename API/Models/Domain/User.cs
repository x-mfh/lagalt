﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("User")]
    public class User
    {
        public Guid Id { get; set; }
        [Required]
        [EmailAddress]
        [MaxLength(100)]
        public string Email { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(100)]
        public string Username { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        [Url]
        public string ImageUrl { get; set; }
        public bool IsSkillsHidden { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public ICollection<Skill> Skills { get; set; }
        public ICollection<UserHistory> UserHistories { get; set; }
        public ICollection<UserPortfolio> UserPortfolios { get; set; }
        public ICollection<ProjectApplication> ProjectApplications { get; set; }

    }
}
