﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("UserProject")]
    public class UserProject
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public int UserProjectRoleId { get; set; }
        public UserProjectRole ProjectRole { get; set; }
        public ICollection<Skill> Skills { get; set; }

    }
}
