﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("Project")]
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string RepoURL { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public Guid CreatorId { get; set; }
        public User Creator { get; set; }


        public int ProgressId { get; set; }
        public ProjectProgress Progress { get; set; }
        public ICollection<UserProject> UserProjects { get; set; }
        public ICollection<ProjectSkill> Skills { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public ICollection<ProjectApplication> ProjectApplications { get; set; }
    }
}
