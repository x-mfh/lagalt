﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("Tag")]
    public class Tag
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public int SkillId { get; set; }
        public Skill Skill { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
