﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("ProjectApplication")]
    public class ProjectApplication
    {
        public int Id { get; set; }
        [Required]
        public string MotivationalText { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsPending { get; set; } = true;
        public bool IsActive { get; set; } = true;
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public Guid UserId { get; set; }
        public User User { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public ICollection<Skill> Skills { get; set; }
    }
}
