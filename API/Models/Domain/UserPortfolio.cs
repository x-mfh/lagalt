﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("UserPortfolio")]
    public class UserPortfolio
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        [Url]
        public string PortfolioURL { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
