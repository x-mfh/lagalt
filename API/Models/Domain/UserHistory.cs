﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("UserHistory")]
    public class UserHistory
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int UserHistoryActionId { get; set; }
        public UserHistoryAction UserHistoryAction { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }

    class UserHistoryComparer : IEqualityComparer<UserHistory>
    {
        public bool Equals(UserHistory x, UserHistory y)
        {

            if (ReferenceEquals(x, y)) return true;

            if (x is null || y is null)
                return false;

            return x.ProjectId == y.ProjectId && x.UserId == y.UserId && x.UserHistoryActionId == y.UserHistoryActionId;
        }


        public int GetHashCode(UserHistory userHistory)
        {
            if (userHistory is null) return 0;

            int hashUserHistoryProjectId = userHistory.ProjectId.GetHashCode();
            int hashUserHistoryUserId = userHistory.UserId.GetHashCode();
            int hashUserHistoryUserHistoryActionId = userHistory.UserHistoryActionId.GetHashCode();

            return hashUserHistoryProjectId ^ hashUserHistoryUserId ^ hashUserHistoryUserHistoryActionId;
        }
    }
}
