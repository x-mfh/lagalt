﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("UserProjectRole")]
    public class UserProjectRole
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
