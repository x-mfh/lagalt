﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("Skill")]
    public class Skill
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<UserProject> UserProjects { get; set; }
        public ICollection<ProjectApplication> ProjectApplications { get; set; }
    }
}
