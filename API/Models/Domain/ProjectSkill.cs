﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models.Domain
{
    [Table("ProjectSkill")]
    public class ProjectSkill
    {
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
        [Required]
        public int RequiredCount { get; set; }
        [Required]
        public int FoundCount { get; set; }
    }
}
