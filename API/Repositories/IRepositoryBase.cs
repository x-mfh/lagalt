﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories
{
    public interface IRepositoryBase<T, ID>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(ID id);
        Task AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(ID id);
        Task<bool> EntityExistsAsync(ID id);
    }
}
