﻿using API.Models.Domain;

namespace API.Repositories.TagRepo
{
    public interface ITagRepository : IRepositoryBase<Tag, int>
    {
    }
}
