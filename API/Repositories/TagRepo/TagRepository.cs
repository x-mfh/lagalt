﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.TagRepo
{
    public class TagRepository : RepositoryBase<Tag, int>, ITagRepository
    {
        public TagRepository(LagaltDbContext context) : base(context)
        {
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.Tags.AnyAsync(e => e.Id == id);
        }

        public override async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await _context.Tags
                .Include(t => t.Skill)
                .ToListAsync();

        }

        public override async Task<Tag> GetByIdAsync(int id)
        {
            return await _context.Tags
                .Include(t => t.Skill)
                .FirstOrDefaultAsync(t => t.Id == id);
        }
    }
}
