﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models.Domain;

namespace API.Repositories.UserRepo
{
    public interface IUserRepository : IRepositoryBase<User, Guid>
    {
        public Task<ICollection<UserHistory>> GetUserHistoriesByUsernameAsync(string username);
        public Task AddUserHistoryByUserIdAsync(Guid userId, UserHistory userHistory);
        public Task<User> GetByUsernameAsync(string username, bool isCurrentLoggedInUser);
        public Task<ICollection<UserProject>> GetAllUserProjectsByUsernameAsync(string username);
        public Task<bool> UserSkillExistsAsync(Guid userId, int skillId);
        public Task<UserPortfolio> GetUserPortfolioByIdAsync(int userPortfolioId);
        public Task UpdateUserSkillsAsync(Guid userid, int[] skillIds);
        public Task AddUserPortfolioAsync(Guid userid, UserPortfolio userPortfolio);
        public Task UpdateUserPortfolioAsync(UserPortfolio userPortfolio);
        public Task RemoveUserPortfolioAsync(Guid userid, UserPortfolio userPortfolio);
        public Task<bool> UsernameExistsAsync(string username);
        public Task<bool> IsCurrentUserAsync(Guid userId, string username);
        public Task<ICollection<ProjectApplication>> GetAllProjectApplicationsByUsernameAsync(string username);
    }
}
