﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.UserRepo
{
    public class UserRepository : RepositoryBase<User, Guid>, IUserRepository
    {
        public UserRepository(LagaltDbContext context) : base(context)
        {

        }

        public override async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Users
                .Include(u => u.Skills)
                .ToListAsync();
        }

        public async Task<ICollection<UserProject>> GetAllUserProjectsByUsernameAsync(string username)
        {
            return await _context.UserProjects
                .Include(up => up.Skills)
                .Include(up => up.Project)
                .Include(up => up.ProjectRole)
                .Where(up => up.User.Username == username).ToListAsync();
        }

        public override async Task<User> GetByIdAsync(Guid id)
        {
            return await _context.Users
                .Include(u => u.Skills)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetByUsernameAsync(string username, bool isCurrentLoggedInUser)
        {
            var user = await _context.Users
                .Include(u => u.UserHistories)
                .Include(u => u.Skills)
                .Include(u => u.UserPortfolios)
                .FirstOrDefaultAsync(u => u.Username == username);

            if (user == null) return user;
            if (user.IsSkillsHidden && isCurrentLoggedInUser == false)
            {
                user.Skills = null;
            }

            return user;
        }

        public override async Task<bool> EntityExistsAsync(Guid id)
        {
            return await _context.Users.AnyAsync(e => e.Id == id);
        }

        public async Task<ICollection<UserHistory>> GetUserHistoriesByUsernameAsync(string username)
        {
            var histories = await _context.UserHistories
                .Include(uh => uh.Project)
                .Include(uh => uh.UserHistoryAction)
                .Where(uh =>
                    uh.User.Username == username
                    && uh.UserHistoryAction.Name == "Viewed")
                .OrderByDescending(uh => uh.CreatedAt)
                .ToListAsync();

            histories = histories.Distinct(new UserHistoryComparer()).Take(5).ToList();

            return histories;
        }

        public async Task AddUserHistoryByUserIdAsync(Guid userId, UserHistory userHistory)
        {
            User userToAddHistory = await _context.Users
                .Include(u => u.UserHistories)
                .Where(u => u.Id == userId)
                .FirstAsync();

            userToAddHistory.UserHistories.Add(userHistory);

            await _context.SaveChangesAsync();
        }

        public async Task<bool> UserSkillExistsAsync(Guid userId, int skillId)
        {
            return await _context.Users.AnyAsync(u => u.Id == userId && u.Skills.Any(s => s.Id == skillId));
        }

        public async Task UpdateUserSkillsAsync(Guid userid, int[] skillIds)
        {
            User userToUpdateSkills = await _context.Users
                .Include(u => u.Skills)
                .Where(u => u.Id == userid)
                .FirstAsync();

            List<Skill> skills = new();

            foreach (int skillId in skillIds)
            {
                Skill skill = await _context.Skills.FindAsync(skillId);
                if (skill == null)
                    throw new KeyNotFoundException();
                skills.Add(skill);
            }
            userToUpdateSkills.Skills = skills;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateUserPortfolioAsync(UserPortfolio userPortfolio)
        {
            _context.Entry(userPortfolio).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> UsernameExistsAsync(string username)
        {
            return await _context.Users.AnyAsync(u => u.Username == username);
        }

        public async Task<bool> IsCurrentUserAsync(Guid userId, string username)
        {
            return await _context.Users.AnyAsync(u => u.Id == userId && u.Username == username);
        }

        public async Task<ICollection<ProjectApplication>> GetAllProjectApplicationsByUsernameAsync(string username)
        {
            return await _context.ProjectApplications
                .Include(pa => pa.Skills)
                .Include(pa => pa.User)
                .Include(pa => pa.Project)
                .Where(pa => pa.User.Username == username && pa.IsActive)
                .ToListAsync();
        }

        public async Task AddUserPortfolioAsync(Guid userid, UserPortfolio userPortfolio)
        {
            User userToAddUserPortfolio = await _context.Users
                .Include(u => u.UserPortfolios)
                .Where(u => u.Id == userid)
                .FirstAsync();

            userToAddUserPortfolio.UserPortfolios.Add(userPortfolio);

            await _context.SaveChangesAsync();
        }

        public async Task RemoveUserPortfolioAsync(Guid userid, UserPortfolio userPortfolio)
        {
            User userToRemoveUserPortfolio = await _context.Users
                .Include(u => u.UserPortfolios)
                .Where(u => u.Id == userid)
                .FirstAsync();

            userToRemoveUserPortfolio.UserPortfolios.Remove(userPortfolio);

            await _context.SaveChangesAsync();
        }

        public async Task<UserPortfolio> GetUserPortfolioByIdAsync(int userPortfolioId)
        {
            return await _context.UserPortfolios.FindAsync(userPortfolioId);
        }
    }
}
