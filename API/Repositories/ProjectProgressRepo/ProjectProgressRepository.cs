﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.ProjectProgressRepo
{
    public class ProjectProgressRepository : RepositoryBase<ProjectProgress, int>, IProjectProgressRepository
    {
        public ProjectProgressRepository(LagaltDbContext context) : base(context)
        {
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.ProjectProgresses.AnyAsync(e => e.Id == id);
        }

        public override async Task<IEnumerable<ProjectProgress>> GetAllAsync()
        {
            return await _context.ProjectProgresses.ToListAsync();

        }

        public override async Task<ProjectProgress> GetByIdAsync(int id)
        {
            return await _context.ProjectProgresses.FindAsync(id);
        }
    }
}
