﻿using API.Models.Domain;

namespace API.Repositories.ProjectProgressRepo
{
    public interface IProjectProgressRepository : IRepositoryBase<ProjectProgress, int>
    {
    }
}
