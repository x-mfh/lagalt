﻿using API;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories
{
    public abstract class RepositoryBase<T, ID> : IRepositoryBase<T, ID> where T : class
    {
        protected readonly LagaltDbContext _context;
        public RepositoryBase(LagaltDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all the entities for the specific table.
        /// </summary>
        /// <returns>A list of entities.</returns>
        public abstract Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Gets a specific entity for the table by their id.
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns>A single entity.</returns>
        public abstract Task<T> GetByIdAsync(ID id);

        /// <summary>
        /// Add a entity to the database.
        /// </summary>
        /// <param name="entity">Entity to add.</param>
        /// <returns></returns>
        public async Task AddAsync(T entity)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the entity.
        /// </summary>
        /// <param name="entity">Entity to update.</param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the entity from the database.
        /// </summary>
        /// <param name="id">Id of the entity to delete.</param>
        /// <returns></returns>
        public async Task DeleteAsync(ID id)
        {
            var foundEntity = await _context.Set<T>().FindAsync(id);
            _context.Set<T>().Remove(foundEntity);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks if an entity exists in the specific table.
        /// </summary>
        /// <param name="id">Id of the entity.</param>
        /// <returns></returns>
        public abstract Task<bool> EntityExistsAsync(ID id);
    }
}
