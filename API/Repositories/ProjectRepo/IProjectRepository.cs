﻿using API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.ProjectRepo
{
    public interface IProjectRepository : IRepositoryBase<Project, int>
    {
        public Task<IEnumerable<Project>> GetAllAsync(string skills);
        public Task<IEnumerable<UserProject>> GetAllUsersByProjectIdAsync(int projectId);
        public Task<IEnumerable<Message>> GetAllMessagesByProjectIdAsync(int projectId);
        public Task AddMessageToProjectAsync(int projectId, Message message);
        public Task<IEnumerable<ProjectApplication>> GetAllPendingApplicationsByProjectIdAsync(int projectId);
        public Task<bool> UserProjectExistsAsync(int projectId, Guid userId);
        public Task<bool> PendingProjectApplicationForProjectExistsAsync(int projectId, Guid userId);
        public Task<bool> ProjectSkillExistsAsync(int projectId, int skillId);
        public Task<bool> ProjectApplicationExistsAsync(int applicationId);
        public Task<bool> CurrentUserIsProjectCreatorAsync(int applicationId, Guid userId);
        public Task AddUserProjectAsync(int projectId, UserProject userProject);
        public Task UpdateUserProjectsAsync(int projectId, List<UserProject> userProjects);
        public Task UpdateProjectApplicationAsync(ProjectApplication projectApplication);
        public Task AddProjectApplicationAsync(ProjectApplication projectApplication);
        public Task<ProjectApplication> GetApplicationByIdAsync(int applicationId);
        public Task<IEnumerable<Project>> GetAllProjectsBySearchString(string searchString);
        public Task<IEnumerable<Project>> GetRecommendedProjectsByUsername(string username);
    }
}
