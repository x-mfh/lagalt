﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.ProjectRepo
{
    public class ProjectRepository : RepositoryBase<Project, int>, IProjectRepository
    {
        public ProjectRepository(LagaltDbContext context) : base(context)
        {
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.Projects.AnyAsync(e => e.Id == id);
        }

        public override async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await _context.Projects
                .Include(p => p.Creator)
                .Include(p => p.Tags)
                .Include(p => p.Progress)
                .Include(p => p.Skills).ThenInclude(ps => ps.Skill)
                .OrderByDescending(p => p.CreatedAt)
                .ToListAsync();
        }

        public async Task<IEnumerable<Project>> GetAllAsync(string skills)
        {
            return await _context.Projects
                .Include(p => p.Creator)
                .Include(p => p.Tags)
                .Include(p => p.Progress)
                .Include(p => p.Skills).ThenInclude(ps => ps.Skill)
                .Where(p => p.Skills.Any(s => skills.Contains(s.Skill.Name)))
                .OrderByDescending(p => p.CreatedAt)
                .ToListAsync();
        }

        public override async Task<Project> GetByIdAsync(int id)
        {
            return await _context.Projects
                .Include(p => p.Creator)
                .Include(p => p.Tags)
                .Include(p => p.Progress)
                .Include(p => p.Skills).ThenInclude(ps => ps.Skill)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IEnumerable<Project>> GetAllProjectsBySearchString(string searchString)
        {
            string[] searchWords = searchString.Split(' ');
            List<Project> searchedProjects = new List<Project>();
            foreach (string searchWord in searchWords)
            {
                searchedProjects.AddRange(await _context.Projects
                     .Include(p => p.Creator)
                     .Include(p => p.Tags)
                     .Include(p => p.Progress)
                     .Include(p => p.Skills).ThenInclude(ps => ps.Skill)
                     .Where(p => p.Title.Contains(searchWord) || p.Creator.Name.Contains(searchWord) || p.Description.Contains(searchWord) || p.Skills.Select(ps => ps.Skill.Name).Contains(searchWord))
                     .ToListAsync());
            }
            return searchedProjects.Distinct();
        }

        public class SkillCounter
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Counter { get; set; }
        }

        public async Task<IEnumerable<Project>> GetRecommendedProjectsByUsername(string username)
        {
            var userProjectsFromHistory = await _context.UserHistories
                .Where(uh => uh.User.Username == username)
                .Include(uh => uh.Project).ThenInclude(p => p.Skills).ThenInclude(ps => ps.Skill)
                .Select(uh => uh.Project)
                .ToListAsync();

            userProjectsFromHistory = userProjectsFromHistory.Distinct().ToList();
            List<SkillCounter> amountOfSkillsInHistory = new List<SkillCounter>();

            foreach (Project project in userProjectsFromHistory)
            {
                foreach (var skill in project.Skills)
                {
                    if (!amountOfSkillsInHistory.Select(x => x.Name).ToList().Contains(skill.Skill.Name))
                    {
                        amountOfSkillsInHistory.Add(new SkillCounter() { Id = skill.Skill.Id, Name = skill.Skill.Name, Counter = 1 });
                    }
                    else
                    {
                        int index = amountOfSkillsInHistory.FindIndex(x => x.Name == skill.Skill.Name);
                        amountOfSkillsInHistory[index].Counter++;
                    }
                }
            }

            amountOfSkillsInHistory = amountOfSkillsInHistory.OrderByDescending(x => x.Counter).ToList();

            List<Project> outputProjects = new List<Project>();
            if(amountOfSkillsInHistory.Count != 0)
            {
                if (amountOfSkillsInHistory.Count > 1)
                {
                    foreach (SkillCounter skillCounter in amountOfSkillsInHistory)
                    {
                        outputProjects.AddRange(await _context.Projects
                            .Include(p => p.Creator)
                            .Include(p => p.Tags)
                            .Include(p => p.Progress)
                            .Include(p => p.Skills).ThenInclude(ps => ps.Skill)
                            .Where(p => p.Skills.Select(ps => ps.SkillId).Contains(skillCounter.Id) && p.Creator.Username != username && !p.UserProjects.Select(up => up.User.Username).Contains(username))
                            .Take(5)
                            .ToListAsync());
                    }
                    outputProjects = outputProjects.OrderByDescending(p => p.CreatedAt).ToList();
                }
                else
                {
                    outputProjects = await _context.Projects
                        .Include(p => p.Creator)
                        .Include(p => p.Tags)
                        .Include(p => p.Progress)
                        .Include(p => p.Skills).ThenInclude(ps => ps.Skill)
                        .Take(10)
                        .Where(p => p.Skills.Select(ps => ps.SkillId).Contains(amountOfSkillsInHistory[0].Id) && p.Creator.Username != username && !p.UserProjects.Select(up => up.User.Username).Contains(username))
                        .OrderByDescending(p => p.CreatedAt)
                        .ToListAsync();
                }
            }            

            return outputProjects.Distinct();
        }

        /// <summary>
        /// Gets all messages associated with a specific project
        /// </summary>
        /// <param name="projectId">Id of the project</param>
        /// <returns>A list of messages</returns>
        public async Task<IEnumerable<Message>> GetAllMessagesByProjectIdAsync(int projectId)
        {
            return await _context.Messages.Include(m => m.User).Where(m => m.ProjectId == projectId && m.IsActive).ToListAsync();
        }

        /// <summary>
        /// Gets all applications for a specific project
        /// </summary>
        /// <param name="projectId">Id of the project</param>
        /// <returns>A list of ProjectApplication</returns>
        public async Task<IEnumerable<ProjectApplication>> GetAllPendingApplicationsByProjectIdAsync(int projectId)
        {
            return await _context.ProjectApplications
                .Include(pa => pa.Skills)
                .Include(pa => pa.User)
                .Include(pa => pa.Project)
                .Where(pa => pa.ProjectId == projectId && pa.IsActive && pa.IsPending)
                .ToListAsync();
        }

        /// <summary>
        /// Gets all users associated with a specific project
        /// </summary>
        /// <param name="projectId">Id of the project</param>
        /// <returns>A list of users</returns>
        public async Task<IEnumerable<UserProject>> GetAllUsersByProjectIdAsync(int projectId)
        {
            return await _context.UserProjects
                .Include(up => up.User)
                .Include(up => up.Skills)
                .Include(up => up.ProjectRole)
                .Where(up => up.ProjectId == projectId && up.UserId != up.Project.CreatorId).AsNoTracking()
                .ToListAsync();
        }

        public async Task<bool> UserProjectExistsAsync(int projectId, Guid userId)
        {
            return await _context.UserProjects.AnyAsync(up => up.ProjectId == projectId && up.UserId == userId);
        }

        public async Task AddUserProjectAsync(int projectId, UserProject userProject)
        {
            Project projectToAddUserProject = await _context.Projects
                .Include(p => p.UserProjects)
                .Include(p => p.Skills)
                .Where(p => p.Id == projectId)
                .FirstAsync();
            // need validation
            projectToAddUserProject.UserProjects.Add(userProject);

            foreach (var skill in userProject.Skills)
            {
                var countToUpdate = projectToAddUserProject.Skills.FirstOrDefault(s => s.SkillId == skill.Id);
                countToUpdate.FoundCount += 1;
            }

            await _context.SaveChangesAsync();
        }

        public async Task UpdateUserProjectsAsync(int projectId, List<UserProject> userProjects)
        {
            Project projectToAddUserProject = await _context.Projects
                .Include(p => p.UserProjects)
                .Include(p => p.Skills)
                .Where(p => p.Id == projectId)
                .FirstAsync();
            // need validation
            projectToAddUserProject.UserProjects = userProjects;

            foreach (var skill in projectToAddUserProject.Skills)
            {
                skill.FoundCount = 0;
            }

            foreach (var userProject in projectToAddUserProject.UserProjects)
            {
                foreach (var skill in userProject.Skills)
                {
                    var countToUpdate = projectToAddUserProject.Skills.FirstOrDefault(s => s.SkillId == skill.Id);
                    countToUpdate.FoundCount += 1;
                }
            }
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ProjectApplicationExistsAsync(int applicationId)
        {
            return await _context.ProjectApplications.AnyAsync(pa => pa.Id == applicationId);
        }

        public async Task UpdateProjectApplicationAsync(ProjectApplication projectApplication)
        {
            _context.Entry(projectApplication).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task AddProjectApplicationAsync(ProjectApplication projectApplication)
        {
            _context.ProjectApplications.Add(projectApplication);
            await _context.SaveChangesAsync();
        }

        public async Task<ProjectApplication> GetApplicationByIdAsync(int applicationId)
        {
            return await _context.ProjectApplications.AsNoTracking().Include(pa => pa.User).Include(pa => pa.Skills).FirstOrDefaultAsync(pa => pa.Id == applicationId);
        }

        public async Task<bool> ProjectSkillExistsAsync(int projectId, int skillId)
        {
            return await _context.ProjectSkills.AnyAsync(ps => ps.ProjectId == projectId && ps.SkillId == skillId);
        }

        public async Task<bool> PendingProjectApplicationForProjectExistsAsync(int projectId, Guid userId)
        {
            return await _context.ProjectApplications.AnyAsync(pa =>
            pa.IsPending
            && pa.ProjectId == projectId
            && pa.UserId == userId);
        }

        public async Task AddMessageToProjectAsync(int projectId, Message message)
        {
            Project projectToAddMessage = await _context.Projects
                .Include(p => p.Messages)
                .Where(p => p.Id == projectId)
                .FirstAsync();

            projectToAddMessage.Messages.Add(message);

            await _context.SaveChangesAsync();
        }

        public async Task<bool> CurrentUserIsProjectCreatorAsync(int applicationId, Guid userId)
        {
            return await _context.Projects.AnyAsync(p => p.Id == applicationId && p.CreatorId == userId);
        }
    }
}