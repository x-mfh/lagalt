﻿using API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.UserHistoryActionRepo
{
    public interface IUserHistoryActionRepository : IRepositoryBase<UserHistoryAction, int>
    {
    }
}
