﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories.UserHistoryActionRepo
{
    public class UserHistoryActionRepository : RepositoryBase<UserHistoryAction, int>, IUserHistoryActionRepository
    {
        public UserHistoryActionRepository(LagaltDbContext context) : base(context)
        {
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.UserHistoryActions.AnyAsync(e => e.Id == id);
        }

        public override async Task<IEnumerable<UserHistoryAction>> GetAllAsync()
        {
            return await _context.UserHistoryActions.ToListAsync();

        }

        public override async Task<UserHistoryAction> GetByIdAsync(int id)
        {
            return await _context.UserHistoryActions.FindAsync(id);
        }
    }
}
