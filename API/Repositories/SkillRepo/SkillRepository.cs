﻿using API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories.SkillRepo
{
    public class SkillRepository : RepositoryBase<Skill, int>, ISkillRepository
    {
        public SkillRepository(LagaltDbContext context) : base(context)
        {
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.Skills.AnyAsync(e => e.Id == id);
        }

        public override async Task<IEnumerable<Skill>> GetAllAsync()
        {
            return await _context.Skills.ToListAsync();

        }

        public override async Task<Skill> GetByIdAsync(int id)
        {
            return await _context.Skills.FindAsync(id);
        }
    }
}
