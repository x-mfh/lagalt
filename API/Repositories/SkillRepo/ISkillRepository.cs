﻿using API.Models.Domain;

namespace API.Repositories.SkillRepo
{
    public interface ISkillRepository : IRepositoryBase<Skill, int>
    {
    }
}
