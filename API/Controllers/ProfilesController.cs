﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using API.Repositories.UserRepo;
using System.Net.Mime;
using AutoMapper;
using API.Models.DTO.UserDTO;
using API.Models.DTO.UserHistoryDTO;
using API.Models.DTO.UserProjectDTO;
using Microsoft.AspNetCore.Authorization;
using API.Helpers;
using API.Models.DTO.ProjectApplicationDTO;

namespace API.Controllers
{
    [Route("api/v1/profiles/{username}")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class ProfilesController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public ProfilesController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<UserReadDTO>> GetUserByUsername(string username)
        {
            bool isCurrentUser = await CheckLoggedInUser(username);
            var user = await _userRepository.GetByUsernameAsync(username, isCurrentUser);
            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<UserReadDTO>(user);
        }

        private async Task<bool> CheckLoggedInUser(string username)
        {
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            bool isCurrentUser = false;
            if (claim != null)
            {
                isCurrentUser = await _userRepository.IsCurrentUserAsync(Guid.Parse(claim.Value), username);
            }

            return isCurrentUser;
        }

        [HttpGet("applications")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ProjectApplicationReadDTO>>> GetProjectApplications(string username)
        {
            if (!await _userRepository.UsernameExistsAsync(username))
            {
                return NotFound();
            }
            return _mapper.Map<List<ProjectApplicationReadDTO>>(await _userRepository.GetAllProjectApplicationsByUsernameAsync(username));
        }

        [HttpGet("projects")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<UserProjectReadDTO>>> GetUserProjects(string username)
        {
            if (!await _userRepository.UsernameExistsAsync(username))
            {
                return NotFound();
            }
            return _mapper.Map<List<UserProjectReadDTO>>(await _userRepository.GetAllUserProjectsByUsernameAsync(username));
        }

        [HttpGet("history")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<UserHistoryReadDTO>>> GetUserHistories(string username)
        {
            if (!await _userRepository.UsernameExistsAsync(username))
            {
                return NotFound();
            }
            return _mapper.Map<List<UserHistoryReadDTO>>(await _userRepository.GetUserHistoriesByUsernameAsync(username));
        }
    }
}
