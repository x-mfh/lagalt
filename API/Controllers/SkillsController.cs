﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API;
using API.Models.Domain;
using System.Net.Mime;
using API.Repositories.SkillRepo;
using AutoMapper;
using API.Models.DTO.SkillDTO;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Route("api/v1/skills")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class SkillsController : ControllerBase
    {
        private readonly ISkillRepository _skillRepository;
        private readonly IMapper _mapper;

        public SkillsController(
            ISkillRepository skillRepository,
            IMapper mapper)
        {
            _skillRepository = skillRepository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SkillReadDTO>>> GetSkills()
        {
            return _mapper.Map<List<SkillReadDTO>>(await _skillRepository.GetAllAsync());
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<SkillReadDTO>> GetSkill(int id)
        {
            var skill = await _skillRepository.GetByIdAsync(id);

            if (skill == null)
            {
                return NotFound();
            }

            return _mapper.Map<SkillReadDTO>(skill);
        }

        [HttpPost]
        public async Task<ActionResult<SkillReadDTO>> PostSkill(SkillCreateDTO dtoSkill)
        {
            var domainSkill = _mapper.Map<Skill>(dtoSkill);
            await _skillRepository.AddAsync(domainSkill);

            return CreatedAtAction("GetSkill",
                new { id = domainSkill.Id },
                _mapper.Map<SkillReadDTO>(domainSkill));

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutSkill(int id, SkillEditDTO dtoSkill)
        {
            if (id != dtoSkill.Id)
            {
                return BadRequest();
            }

            if (!await _skillRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            Skill domainSkill = _mapper.Map<Skill>(dtoSkill);
            await _skillRepository.UpdateAsync(domainSkill);

            return NoContent();

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteSkill(int id)
        {
            if (!await _skillRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            await _skillRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
