﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using System.Net.Mime;
using API.Repositories.ProjectProgressRepo;
using AutoMapper;
using API.Models.DTO.ProjectProgressDTO;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Route("api/v1/projectprogress")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class ProjectProgressesController : ControllerBase
    {
        private readonly IProjectProgressRepository _projectProgressRepository;
        private readonly IMapper _mapper;

        public ProjectProgressesController(
            IProjectProgressRepository projectProgressRepository,
            IMapper mapper)
        {
            _projectProgressRepository = projectProgressRepository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectProgressReadDTO>>> GetProjectProgresses()
        {
            return _mapper.Map<List<ProjectProgressReadDTO>>(await _projectProgressRepository.GetAllAsync());
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectProgressReadDTO>> GetProjectProgress(int id)
        {
            var projectProgress = await _projectProgressRepository.GetByIdAsync(id);

            if (projectProgress == null)
            {
                return NotFound();
            }

            return _mapper.Map<ProjectProgressReadDTO>(projectProgress);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectProgressReadDTO>> PostProjectProgress(ProjectProgressCreateDTO dtoProjectProgress)
        {
            var domainProjectProgress = _mapper.Map<ProjectProgress>(dtoProjectProgress);
            await _projectProgressRepository.AddAsync(domainProjectProgress);

            return CreatedAtAction("GetProjectProgress",
                new { id = domainProjectProgress.Id },
                _mapper.Map<ProjectProgressReadDTO>(domainProjectProgress));

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutProjectProgress(int id, ProjectProgressEditDTO dtoProjectProgress)
        {
            if (id != dtoProjectProgress.Id)
            {
                return BadRequest();
            }

            if (!await _projectProgressRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            ProjectProgress domainProjectProgress = _mapper.Map<ProjectProgress>(dtoProjectProgress);
            await _projectProgressRepository.UpdateAsync(domainProjectProgress);

            return NoContent();

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProjectProgress(int id)
        {
            if (!await _projectProgressRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            await _projectProgressRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
