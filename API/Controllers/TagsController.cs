﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using API.Repositories.TagRepo;
using AutoMapper;
using API.Models.DTO.TagDTO;

namespace API.Controllers
{
    [Route("api/v1/tags")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class TagsController : ControllerBase
    {
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;

        public TagsController(
            ITagRepository tagRepository,
            IMapper mapper)
        {
            _tagRepository = tagRepository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagReadDTO>>> GetTags()
        {
            return _mapper.Map<List<TagReadDTO>>(await _tagRepository.GetAllAsync());
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<TagReadDTO>> GetTag(int id)
        {
            var projectProgress = await _tagRepository.GetByIdAsync(id);

            if (projectProgress == null)
            {
                return NotFound();
            }

            return _mapper.Map<TagReadDTO>(projectProgress);
        }

        [HttpPost]
        public async Task<ActionResult<TagReadDTO>> PostTag(TagCreateDTO dtoTag)
        {
            var domainTag = _mapper.Map<Tag>(dtoTag);
            await _tagRepository.AddAsync(domainTag);

            return CreatedAtAction("GetTag",
                new { id = domainTag.Id },
                _mapper.Map<TagReadDTO>(domainTag));

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutTag(int id, TagEditDTO dtoTag)
        {
            if (id != dtoTag.Id)
            {
                return BadRequest();
            }

            if (!await _tagRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            Tag domainTag = _mapper.Map<Tag>(dtoTag);
            await _tagRepository.UpdateAsync(domainTag);

            return NoContent();

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTag(int id)
        {
            if (!await _tagRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            await _tagRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
