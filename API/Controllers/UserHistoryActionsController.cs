﻿using API.Models.Domain;
using API.Models.DTO.UserHistoryActionDTO;
using API.Repositories.UserHistoryActionRepo;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/v1/userhistoryactions")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class UserHistoryActionsController : ControllerBase
    {
        private readonly IUserHistoryActionRepository _userHistoryActionRepository;
        private readonly IMapper _mapper;

        public UserHistoryActionsController(
            IUserHistoryActionRepository userHistoryActionRepository,
            IMapper mapper)
        {
            _userHistoryActionRepository = userHistoryActionRepository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserHistoryActionReadDTO>>> GetUserHistoryActions()
        {
            return _mapper.Map<List<UserHistoryActionReadDTO>>(await _userHistoryActionRepository.GetAllAsync());
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserHistoryActionReadDTO>> GetUserHistoryAction(int id)
        {
            var userHistoryAction = await _userHistoryActionRepository.GetByIdAsync(id);

            if (userHistoryAction == null)
            {
                return NotFound();
            }

            return _mapper.Map<UserHistoryActionReadDTO>(userHistoryAction);
        }

        [HttpPost]
        public async Task<ActionResult<UserHistoryActionReadDTO>> PostUserHistoryAction(UserHistoryActionCreateDTO dtoUserHistoryAction)
        {
            var domainUserHistoryAction = _mapper.Map<UserHistoryAction>(dtoUserHistoryAction);
            await _userHistoryActionRepository.AddAsync(domainUserHistoryAction);

            return CreatedAtAction("GetUserHistoryAction",
                new { id = domainUserHistoryAction.Id },
                _mapper.Map<UserHistoryActionReadDTO>(domainUserHistoryAction));

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutUserHistoryAction(int id, UserHistoryActionEditDTO dtoUserHistoryAction)
        {
            if (id != dtoUserHistoryAction.Id)
            {
                return BadRequest();
            }

            if (!await _userHistoryActionRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            UserHistoryAction domainUserHistoryAction = _mapper.Map<UserHistoryAction>(dtoUserHistoryAction);
            await _userHistoryActionRepository.UpdateAsync(domainUserHistoryAction);

            return NoContent();

        }

        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteUserHistoryAction(int id)
        //{
        //    if (!await _userHistoryActionRepository.EntityExistsAsync(id))
        //    {
        //        return NotFound();
        //    }

        //    await _userHistoryActionRepository.DeleteAsync(id);

        //    return NoContent();
        //}
    }
}
