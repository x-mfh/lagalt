﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API;
using API.Models.Domain;
using System.Net.Mime;
using API.Repositories.ProjectRepo;
using AutoMapper;
using API.Models.DTO.ProjectDTO;
using API.Models.DTO.UserDTO;
using API.Models.DTO.MessageDTO;
using API.Models.DTO.UserProjectDTO;
using API.Models.DTO.ProjectApplicationDTO;
using Microsoft.AspNetCore.Authorization;
using API.Helpers;
using API.Repositories.UserRepo;
using API.Repositories.SkillRepo;

namespace API.Controllers
{
    [Route("api/v1/projects")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISkillRepository _skillRepository;
        private readonly IMapper _mapper;

        public ProjectsController(
            IProjectRepository projectRepository,
            IUserRepository userRepository,
            ISkillRepository skillRepository,
            IMapper mapper)
        {
            _projectRepository = projectRepository;
            _userRepository = userRepository;
            _skillRepository = skillRepository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectReadMultipleDTO>>> GetProjects(string skills)
        {
            if (skills != null)
                return _mapper.Map<List<ProjectReadMultipleDTO>>(await _projectRepository.GetAllAsync(skills));
            return _mapper.Map<List<ProjectReadMultipleDTO>>(await _projectRepository.GetAllAsync());
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectReadSingleDTO>> GetProject(int id)
        {
            var project = await _projectRepository.GetByIdAsync(id);

            if (project == null)
            {
                return NotFound();
            }

            return _mapper.Map<ProjectReadSingleDTO>(project);
        }

        [AllowAnonymous]
        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<ProjectReadMultipleDTO>>> GetProjectsBySearch([FromQuery]string searchString)
        {
            var projects = await _projectRepository.GetAllProjectsBySearchString(searchString);

            if(projects == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<ProjectReadMultipleDTO>>(projects);
        }

        [AllowAnonymous]
        [HttpGet("{username}/recommended")]
        public async Task<ActionResult<IEnumerable<ProjectReadMultipleDTO>>> GetRecommendedByUsername(string username)
        {
            var projects = await _projectRepository.GetRecommendedProjectsByUsername(username);
            if(projects == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<ProjectReadMultipleDTO>>(projects);
        }

        [AllowAnonymous]
        [HttpGet("{id}/users")]
        public async Task<ActionResult<IEnumerable<UserProjectReadDTO>>> GetProjectUsers(int id)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<UserProjectReadDTO>>(await _projectRepository.GetAllUsersByProjectIdAsync(id));
        }

        [AllowAnonymous]
        [HttpPost("{id}/users")]
        public async Task<ActionResult<UserProjectReadDTO>> PostProjectUser(int id, UserProjectCreateDTO dtoUserProject)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            UserProject domainUserProject = _mapper.Map<UserProject>(dtoUserProject);
            // default to Contributor for now.
            domainUserProject.UserProjectRoleId = 3;

            await _projectRepository.AddUserProjectAsync(id, domainUserProject);

            return CreatedAtAction("GetProjectUsers",
                new { id, applicationId = domainUserProject.Id },
                _mapper.Map<UserProjectReadDTO>(domainUserProject));
        }

        [HttpPut("{id}/users")]
        public async Task<ActionResult> UpdateProjectUsers(int id, List<UserProjectEditDTO> dtoUserProjects)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            //var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            //var userId = Guid.Parse(claim.Value);

            //if (!await _projectRepository.CurrentUserIsProjectCreatorAsync(id, userId))
            //{
            //    return Unauthorized();
            //}

            // right now i removed id from editDTO, so it will just create a new one.
            // Messes up the date, since a new one is created and old one is deleted.
            List<UserProject> currentUserProjects = (List<UserProject>)await _projectRepository.GetAllUsersByProjectIdAsync(id);

            _mapper.Map(dtoUserProjects, currentUserProjects);

            for (int i = 0; i < currentUserProjects.Count; i++)
            {
                List<Skill> skills = new();
                foreach (var skillId in dtoUserProjects[i].Skills)
                {
                    Skill domainSkill = await _skillRepository.GetByIdAsync(skillId);
                    skills.Add(domainSkill);
                }
                currentUserProjects[i].Skills = skills;
            }

            await _projectRepository.UpdateUserProjectsAsync(id, currentUserProjects);

            return NoContent();
        }

        [AllowAnonymous]
        [HttpGet("{id}/messages")]
        public async Task<ActionResult<IEnumerable<MessageReadDTO>>> GetProjectMessages(int id)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<MessageReadDTO>>(await _projectRepository.GetAllMessagesByProjectIdAsync(id));
        }

        [HttpPost("{id}/messages")]
        public async Task<ActionResult<MessageReadDTO>> PostProjectMessages(int id, MessageCreateDTO dtoMessage)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }
            
            Message domainMessage = _mapper.Map<Message>(dtoMessage);
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            domainMessage.UserId = Guid.Parse(claim.Value);

            await _projectRepository.AddMessageToProjectAsync(id, domainMessage);

            return _mapper.Map<MessageReadDTO>(domainMessage);
        }

        [AllowAnonymous]
        [HttpGet("{id}/applications")]
        public async Task<ActionResult<IEnumerable<ProjectApplicationReadDTO>>> GetProjectApplications(int id)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<ProjectApplicationReadDTO>>(await _projectRepository.GetAllPendingApplicationsByProjectIdAsync(id));
        }

        [AllowAnonymous]
        [HttpGet("{id}/applications/{applicationId}")]
        public async Task<ActionResult<ProjectApplicationReadDTO>> GetProjectApplication(int id, int applicationId)
        {
            if (!await _projectRepository.EntityExistsAsync(id) || !await _projectRepository.ProjectApplicationExistsAsync(applicationId))
            {
                return NotFound();
            }

            return _mapper.Map<ProjectApplicationReadDTO>(await _projectRepository.GetApplicationByIdAsync(applicationId));
        }

        // check if entities exists before creating?
        [HttpPost("{id}/applications")]
        public async Task<ActionResult<ProjectApplicationReadDTO>> PostProjectApplications(int id, ProjectApplicationCreateDTO dtoProjectApplication)
        {
            // check if project exists.
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            if(string.IsNullOrWhiteSpace(dtoProjectApplication.MotivationalText) || dtoProjectApplication.Skills.Count == 0)
            {
                return BadRequest("Input the required fields");
            }

            ProjectApplication domainProjectApplication = new() { 
                ProjectId = id, 
                MotivationalText = dtoProjectApplication.MotivationalText
            };
            
            // get userId from token.
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            if (claim != null)
            {
                var userId = Guid.Parse(claim.Value);
                domainProjectApplication.UserId = userId;
                // check if user already has a pending projectapplication.
                if (await _projectRepository.PendingProjectApplicationForProjectExistsAsync(id, userId))
                {
                    return BadRequest("User already has a pending application on this project.");
                }

                // check if user with userId has the same skills as the skills in the createDto.
                // is it necessary to check if skill exists? prob not
                List<Skill> skills = new();
                foreach (var skillId in dtoProjectApplication.Skills)
                {
                    // check if application is looking for that skill
                    // maybe do same as below, we should check this with entityexists
                    if (!await _projectRepository.ProjectSkillExistsAsync(id, skillId))
                    {
                        return BadRequest("One or more skills doesn't exists on the project.");
                    }

                    // maybe refactor to be UserSkillsExists, with userId and list of skill ids
                    // to prevent looping in the controller.
                    if (!await _userRepository.UserSkillExistsAsync(userId, skillId))
                    {
                        return BadRequest("One or more skills doesn't exists on the user.");
                    }

                    Skill skill = await _skillRepository.GetByIdAsync(skillId);
                    skills.Add(skill);
                }

                domainProjectApplication.Skills = skills;
            }
            else
            {
                return Unauthorized("how did you get here without an id in your token");
            }


            // mapping seems unnecessary, unless it gets refactored and does the stuff above.
            //ProjectApplication domainProjectApplication = _mapper.Map<ProjectApplication>(dtoProjectApplication);
            await _projectRepository.AddProjectApplicationAsync(domainProjectApplication);

            return CreatedAtAction("GetProjectApplication",
                new { id, applicationId = domainProjectApplication.Id },
                _mapper.Map<ProjectApplicationReadDTO>(domainProjectApplication));
        }

        // check if entities exists before updating?
        [HttpPut("{id}/applications/{applicationId}")]
        public async Task<ActionResult> PutProjectApplications(int id, int applicationId, ProjectApplicationEditDTO dtoProjectApplication)
        {
            if (id != dtoProjectApplication.ProjectId || applicationId != dtoProjectApplication.Id)
            {
                return BadRequest();
            }
            ProjectApplication domainProjectApplication = await _projectRepository.GetApplicationByIdAsync(applicationId);

            if (domainProjectApplication == null)
            {
                return NotFound();
            }

            _mapper.Map(dtoProjectApplication, domainProjectApplication);

            await _projectRepository.UpdateProjectApplicationAsync(domainProjectApplication);

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<ProjectReadSingleDTO>> PostProject(ProjectCreateDTO dtoProject)
        {
            Project domainProject = _mapper.Map<Project>(dtoProject);

            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            domainProject.CreatorId = Guid.Parse(claim.Value);

            await _projectRepository.AddAsync(domainProject);

            List<Skill> skills = new();
            await _projectRepository.AddUserProjectAsync(domainProject.Id,
                new UserProject
                {
                    UserProjectRoleId = 1,
                    UserId = domainProject.CreatorId,
                    Skills = skills
                });

            return CreatedAtAction("GetProject",
                new { id = domainProject.Id },
                _mapper.Map<ProjectReadSingleDTO>(domainProject));

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutProject(int id, ProjectEditDTO dtoProject)
        {
            if (id != dtoProject.Id)
            {
                return BadRequest();
            }

            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var userId = Guid.Parse(claim.Value);

            if (!await _projectRepository.CurrentUserIsProjectCreatorAsync(id, userId))
            {
                return Unauthorized();
            }

            Project domainProject = _mapper.Map<Project>(dtoProject);
            domainProject.CreatorId = userId;
            await _projectRepository.UpdateAsync(domainProject);

            return NoContent();

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProject(int id)
        {
            if (!await _projectRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            await _projectRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
