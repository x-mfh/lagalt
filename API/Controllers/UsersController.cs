﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using API.Repositories.UserRepo;
using System.Net.Mime;
using AutoMapper;
using API.Models.DTO.UserDTO;
using API.Models.DTO.UserHistoryDTO;
using API.Models.DTO.UserProjectDTO;
using Microsoft.AspNetCore.Authorization;
using API.Helpers;
using API.Models.DTO.UserPortfolioDTO;

namespace API.Controllers
{
    [Route("api/v1/users")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UsersController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _userRepository.GetAllAsync());
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<UserReadDTO>> GetUser(Guid id)
        {
            var user = await _userRepository.GetByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<UserReadDTO>(user);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> PutUser(Guid id, UserEditDTO dtoUser)
        {
            if (id != dtoUser.Id)
            {
                return BadRequest();
            }

            if(!await _userRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var userId = Guid.Parse(claim.Value);

            if (id != userId)
            {
                return Unauthorized();
            }

            User user = await _userRepository.GetByIdAsync(id);
            _mapper.Map(dtoUser, user);

            await _userRepository.UpdateAsync(user);

            return NoContent();
        }

        [HttpPost("{id}/userportfolios")]
        public async Task<ActionResult<UserPortfolioReadDTO>> PostUserPortFolio(Guid id, UserPortfolioCreateDTO dtoUserPortfolio)
        {
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var userId = Guid.Parse(claim.Value);

            if (id != userId)
            {
                return Unauthorized();
            }

            if (!await _userRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            UserPortfolio domainUserPortfolio = _mapper.Map<UserPortfolio>(dtoUserPortfolio);
            await _userRepository.AddUserPortfolioAsync(id, domainUserPortfolio);

            return Ok(_mapper.Map<UserPortfolioReadDTO>(domainUserPortfolio));
        }

        [HttpPut("{id}/userportfolios/{userPorfolioId}")]
        public async Task<ActionResult> PutUserPortFolio(Guid id, int userPorfolioId, UserPortfolioEditDTO dtoUserPortfolio)
        {
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var userId = Guid.Parse(claim.Value);

            if (id != userId)
            {
                return Unauthorized();
            }

            UserPortfolio domainUserPortfolio = await _userRepository.GetUserPortfolioByIdAsync(userPorfolioId);
            if (!await _userRepository.EntityExistsAsync(id) || domainUserPortfolio == null)
            {
                return NotFound();
            }

            _mapper.Map(dtoUserPortfolio, domainUserPortfolio);

            await _userRepository.UpdateUserPortfolioAsync(domainUserPortfolio);

            return NoContent();
        }

        [HttpDelete("{id}/userportfolios/{userPorfolioId}")]
        public async Task<ActionResult> RemoveUserPortFolio(Guid id, int userPorfolioId)
        {
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var userId = Guid.Parse(claim.Value);

            if (id != userId)
            {
                return Unauthorized();
            }

            UserPortfolio domainUserPortfolio = await _userRepository.GetUserPortfolioByIdAsync(userPorfolioId);
            if (!await _userRepository.EntityExistsAsync(id) || domainUserPortfolio == null)
            {
                return NotFound();
            }

            await _userRepository.RemoveUserPortfolioAsync(id, domainUserPortfolio);

            return NoContent();
        }

        [HttpPut("{id}/skills")]
        public async Task<ActionResult> PutUserSkills(Guid id, int[] skillIds)
        {
            if(!await _userRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var userId = Guid.Parse(claim.Value);

            if (id != userId)
            {
                return Unauthorized();
            }

            try
            {
                await _userRepository.UpdateUserSkillsAsync(id, skillIds);
            } catch(KeyNotFoundException)
            {
                return BadRequest("Invalid Skill");
            }
            return NoContent();
        }
        
        [HttpPost]
        public async Task<ActionResult<UserReadDTO>> PostUser(UserCreateDTO dtoUser)
        {
            var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
            var claimUserId = claim?.Value;
            if (claimUserId == null)
                return BadRequest();
            var domainUser = _mapper.Map<User>(dtoUser);
            domainUser.Id = Guid.Parse(claimUserId);
            await _userRepository.AddAsync(domainUser);

            return CreatedAtAction("GetUser", new { id = domainUser.Id }, _mapper.Map<UserReadDTO>(domainUser));
        }
        
        [HttpPost("{id}/history")]
        public async Task<ActionResult<UserHistoryReadDTO>> PostUserHistory(Guid id, UserHistoryCreateDTO dtoUserHistory)
        {
            var domainUserHistory = _mapper.Map<UserHistory>(dtoUserHistory);
            await _userRepository.AddUserHistoryByUserIdAsync(id, domainUserHistory);

            // createdAt URI/ROUTE takes an username and not id, how to fix?
            //return CreatedAtAction("GetUserHistories", new { id = domainUserHistory.Id }, _mapper.Map<UserHistoryReadDTO>(domainUserHistory));
            return Ok();
        }
    }
}
