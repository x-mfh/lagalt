﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Helpers
{
    public static class TokenHelper
    {
        public static async Task<Claim> GetClaimAsync(HttpContext context, string claimName)
        {
            var accessToken = await context.GetTokenAsync("Bearer", "access_token");
            if (accessToken == null) 
                return null;

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadJwtToken(accessToken);
            var sub = jsonToken.Claims.First(claim => claim.Type == claimName);
            return sub;
        }

        //var claim = await TokenHelper.GetClaimAsync(HttpContext, "sub");
        //var claimUserId = claim?.Value;
    }
}