﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

//[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
//public class AuthorizeAttribute : Attribute, IAuthorizationFilter
//{
//    public async void OnAuthorization(AuthorizationFilterContext context)
//    {
//        var token = await context.HttpContext.GetTokenAsync("access_token");
//        if (token != null)
//        {
//            var handler = new JwtSecurityTokenHandler();
//            var jsonToken = handler.ReadJwtToken(token);
//            var sub = jsonToken.Claims.First(claim => claim.Type == "sub").Value;
//            context.HttpContext.Items.Add("userId", sub);
//        }
//        else
//        {
//            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
//        }
//    }
//    //var userId = HttpContext.Items["userId"];
//}