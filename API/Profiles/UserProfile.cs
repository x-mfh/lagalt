﻿using API.Models.Domain;
using API.Models.DTO.UserDTO;
using AutoMapper;
using System.Linq;

namespace API.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ReverseMap();

            CreateMap<User, UserCreateDTO>()
                .ReverseMap();

            CreateMap<User, UserEditDTO>()
                .ReverseMap();

            CreateMap<User, UserRefReadDTO>()
                .ReverseMap();
        }
    }
}
