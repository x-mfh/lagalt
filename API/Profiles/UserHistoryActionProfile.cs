﻿using API.Models.Domain;
using API.Models.DTO.UserHistoryActionDTO;
using AutoMapper;

namespace API.Profiles
{
    public class UserHistoryActionProfile : Profile
    {
        public UserHistoryActionProfile()
        {
            CreateMap<UserHistoryAction, UserHistoryActionCreateDTO>()
                .ReverseMap();
            CreateMap<UserHistoryAction, UserHistoryActionEditDTO>()
                .ReverseMap();
            CreateMap<UserHistoryAction, UserHistoryActionReadDTO>()
                .ReverseMap();
        }
    }
}
