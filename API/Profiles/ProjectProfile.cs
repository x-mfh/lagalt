﻿using API.Models.Domain;
using API.Models.DTO.ProjectDTO;
using AutoMapper;
using System.Linq;

namespace API.Profiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectReadMultipleDTO>()
                .ForMember(pdto => pdto.Progress, opt => opt
                .MapFrom(p => p.Progress))

                .ForMember(pdto => pdto.Tags, opt => opt
                .MapFrom(p => p.Tags.Select(t => t.Name).ToList()))

                .ForMember(pdto => pdto.Skills, opt => opt
                .MapFrom(p => p.Skills))

                .ForMember(pdto => pdto.Creator, opt => opt
                .MapFrom(p => p.Creator.Username))
                .ReverseMap();

            CreateMap<Project, ProjectReadSingleDTO>()
                .ForMember(pdto => pdto.Progress, opt => opt
                .MapFrom(p => p.Progress))

                .ForMember(pdto => pdto.Tags, opt => opt
                .MapFrom(p => p.Tags.Select(t => t.Name).ToList()))

                .ForMember(pdto => pdto.Skills, opt => opt
                .MapFrom(p => p.Skills))
                .ReverseMap();

            CreateMap<Project, ProjectCreateDTO>()
                .ReverseMap();

            CreateMap<Project, ProjectEditDTO>()
                .ReverseMap();

            CreateMap<ProjectSkill, ProjectSkillReadDTO>()
                .ForMember(psdto => psdto.SkillName, opt => opt
                .MapFrom(ps => ps.Skill.Name))
                .ReverseMap();

            CreateMap<ProjectSkill, ProjectSkillCreateDTO>()
                .ReverseMap();

            CreateMap<Project, ProjectRefReadDTO>()
                .ReverseMap();
        }
    }
}
