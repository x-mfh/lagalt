﻿using API.Models.Domain;
using API.Models.DTO.UserProjectDTO;
using AutoMapper;
using System.Linq;

namespace API.Profiles
{
    public class UserProjectProfile : Profile
    {
        public UserProjectProfile()
        {
            CreateMap<UserProject, UserProjectReadDTO>()
                .ReverseMap();

            CreateMap<UserProject, UserProjectCreateDTO>()
                .ReverseMap();

            CreateMap<UserProjectEditDTO, UserProject>()
                .ForMember(up => up.Skills, opt => opt.Ignore());

            CreateMap<UserProjectRole, UserProjectRoleReadDTO>()
                .ReverseMap();
        }
    }
}
