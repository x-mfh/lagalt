﻿using API.Models.Domain;
using API.Models.DTO.UserPortfolioDTO;
using AutoMapper;

namespace API.Profiles
{
    public class UserPortfolioProfile : Profile
    {
        public UserPortfolioProfile()
        {
            CreateMap<UserPortfolio, UserPortfolioReadDTO>()
                .ReverseMap();

            CreateMap<UserPortfolio, UserPortfolioCreateDTO>()
                .ReverseMap();

            CreateMap<UserPortfolio, UserPortfolioEditDTO>()
                .ReverseMap();
        }
    }
}
