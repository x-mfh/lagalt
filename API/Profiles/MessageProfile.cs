﻿using API.Models.Domain;
using API.Models.DTO.MessageDTO;
using AutoMapper;

namespace API.Profiles
{
    public class MessageProfile : Profile
    {
        public MessageProfile()
        {
            CreateMap<Message, MessageReadDTO>()
                .ReverseMap();

            CreateMap<Message, MessageCreateDTO>()
                .ReverseMap();

            CreateMap<Message, MessageEditDTO>()
                .ReverseMap();
        }
    }
}
