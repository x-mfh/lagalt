﻿using API.Models.Domain;
using API.Models.DTO.UserHistoryDTO;
using AutoMapper;

namespace API.Profiles
{
    public class UserHistoryProfile : Profile
    {
        public UserHistoryProfile()
        {
            CreateMap<UserHistory, UserHistoryReadDTO>()
                .ForMember(uhdto => uhdto.UserHistoryAction, opt => opt
                .MapFrom(uh => uh.UserHistoryAction.Name))
                .ReverseMap();

            CreateMap<UserHistory, UserHistoryCreateDTO>()
                .ReverseMap();
        }
    }
}
