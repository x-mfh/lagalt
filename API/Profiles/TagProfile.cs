﻿using API.Models.Domain;
using API.Models.DTO.TagDTO;
using AutoMapper;

namespace API.Profiles
{
    public class TagProfile : Profile
    {
        public TagProfile()
        {
            CreateMap<Tag, TagReadDTO>()
                .ForMember(tdto => tdto.SkillName, opt => opt
                .MapFrom(t => t.Skill.Name))
                .ReverseMap();

            CreateMap<Tag, TagCreateDTO>()
                .ReverseMap();

            CreateMap<Tag, TagEditDTO>()
                .ReverseMap();
        }
    }
}
