﻿using API.Models.Domain;
using API.Models.DTO.ProjectProgressDTO;
using AutoMapper;

namespace API.Profiles
{
    public class ProjectProgressProfile : Profile
    {
        public ProjectProgressProfile()
        {
            CreateMap<ProjectProgress, ProjectProgressReadDTO>()
                .ReverseMap();

            CreateMap<ProjectProgress, ProjectProgressCreateDTO>()
                .ReverseMap();

            CreateMap<ProjectProgress, ProjectProgressEditDTO>()
                .ReverseMap();
        }
    }
}
