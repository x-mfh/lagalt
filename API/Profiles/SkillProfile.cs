﻿using API.Models.Domain;
using API.Models.DTO.SkillDTO;
using AutoMapper;

namespace API.Profiles
{
    public class SkillProfile : Profile
    {
        public SkillProfile()
        {
            CreateMap<Skill, SkillReadDTO>()
                .ReverseMap();

            CreateMap<Skill, SkillCreateDTO>()
                .ReverseMap();

            CreateMap<Skill, SkillEditDTO>()
                .ReverseMap();
        }
    }
}
