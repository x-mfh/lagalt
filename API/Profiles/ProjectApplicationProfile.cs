﻿using API.Models.Domain;
using API.Models.DTO.ProjectApplicationDTO;
using AutoMapper;
using System.Linq;

namespace API.Profiles
{
    public class ProjectApplicationProfile : Profile
    {
        public ProjectApplicationProfile()
        {
            CreateMap<ProjectApplication, ProjectApplicationReadDTO>()
                .ReverseMap();

            CreateMap<ProjectApplication, ProjectApplicationCreateDTO>()
                .ReverseMap();

            CreateMap<ProjectApplication, ProjectApplicationEditDTO>()
                .ReverseMap();
        }
    }
}
